# TABLE OF CONTENTS  

**This** is what I had in my mind for starting the review(i really tried to shorten the context and be concise):

0. Basics & introduction  

    goals: installing a DB ,Basics ,understanding relational model ,types and history of DBs ,connecting to DBs ,how they work , structure of DBs  
    exercise: nope  
    extras: yes  

1. Data Modification & manipulation  

      goals: inserting,deleting,modifying data(rows),columns ,and tables ,understanding constraints ,and creating DBs  
      exercise: Modify Data  
      extras: no  

2. DB related concepts  

    goals: all kinds of concepts related to working with a DB (like: views, sequences, design, ...)  
    exercise: nope  
    extras: yes  

3. Queries  

    goals: excersice on: query Data using standard SQL languge, from zero to more advanced queries(aggr,subquery,joins,CTE,recursion,...)
    exercise: all remaining parts  
    extras: yes  

4. DataTypes  

    goals: understanding DataTypes and working with kinds of Data  
    exercise: Date  
    extras: yes  

5. Functions  

    goals: Working with functions and triggers  
    exercise: nope  
    extras: no  


## Resources:  

- Postgres SQL documentation  

- Pgexercise.com  

- This course: [Learn SQL Using PostgreSQL](https://www.udemy.com/course/postgresql-from-zero-to-hero/ "this") 

- Google.com

