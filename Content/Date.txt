select timestamp '2012-08-31 01:00';
----------------
// my wrong answer
select timestamp ('2012-08-31 01:00' - '2012-07-30 01:00') ;        
// my second wrong answer
select '2012-08-31 01:00' - '2012-07-30 01:00';
// finally
select timestamp '2012-08-31 01:00' - timestamp '2012-07-30 01:00';
----------------------
/// i didnt know how to do it
select generate_series(timestamp '2012-10-01', timestamp '2012-10-31', interval '1 day') as ts;   
/// just copied and absorbed it
-----------------------
select extract(day from timestamp '2012-08-31');
//i took a glance at documentation, found it there
---------------------
select extract(epoch from (timestamp '2012-09-02 00:00:00' - timestamp '2012-08-31 01:00:00'));
---------------------
select 	extract(month from cal.month) as month,
	(cal.month + interval '1 month') - cal.month as length
	from
	(
		select generate_series(timestamp '2012-01-01', timestamp '2012-12-01', interval '1 month') as month
	) cal
order by month;    
// couldnt solve it by maself
-----------------------
select (date_trunc('month',ts.testts) + interval '1 month') 
		- date_trunc('day', ts.testts)
	from (select timestamp '2012-02-11 01:00:00' as testts) ts
//////////... i had no clue... now i know about date_trunc
---------------------
select starttime, starttime + slots*(interval '30 minutes') as endtime
	from cd.bookings
	order by endtime desc, starttime desc
	limit 10;
-----------------------
select date_trunc('month', starttime) as months ,count(*) from cd.bookings group by months  order by months;
// interesting question
----------------------
/// this one, ill do it in the future
///
///

