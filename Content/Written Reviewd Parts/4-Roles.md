# ROLES

## BASICS

PostgreSQL manages database access permissions using the concept of roles. A role can be thought of as either a database user, or a group of database users(users are roles with passwords).  
Roles can own database objects (for example, tables and functions) and can assign privileges on those objects to other roles to control who has access to which objects.  
it is possible to grant membership in a role to another role.  
in version 8.1 we had users and groups instead of roles.  
Roles can be nested, roles can contain other roles.  
Best way to set it up is: first, creating generic roles without login and then assign users to this roles.  
We have 6 levels of DB roles security, from instance level(all DBs) all the way down to row level of a DB.  
Every level had some attributes or permissions:  


## TOP LEVEL (INSTANCE LEVVEL)

- SUPERUSER: A database superuser bypasses all permission checks, except the right to log in. *dangerous privilege*  

- CREATEDB: A role must be explicitly given permission to create databases (except for superusers)

- CREATEROLE: A role must be explicitly given permission to create more roles (except for superusers), can alter and drop other roles, too, as well as grant or revoke membership in them

- LOGIN :A role with the LOGIN attribute can be considered the same as a “database user”

- REPLICATION: initiating streaming replication

- Password: for login

*every permission can be reversed using no(just the absence of a permission)*  

*Database roles are conceptually completely separate from operating system users. role name must follow the rules for SQL identifiers*  

## SETTING UP

The role identity determines the set of privileges available to a connected client.  
Roles can either have login or not(which makes them group role, group roles give permissions to roles)

```sql
CREATE ROLE name perm;
```

We create users from roles, which has login ability:  

```sql

CREATE USER user-name LOGIN password 'pass'
//or
CREATE ROLE with LOGIN password 'pass'
GRANT role TO user;
GRANT group_role TO role1
//X
REVOKE group_role FROM role1
REVOKE role from user-name
```

*It is good practice to create a role that has the CREATEDB and CREATEROLE privileges, but is not a superuser*  

List of all roles:  

```sql

SELECT rolname FROM pg_roles; // or use \du in psql
```

a freshly initialized system always contains one predefined role. The role is always a “superuser”, and this role will be named postgres or operating system name. In order to create more roles you first have to connect as this initial role.  

Every connection to the database server is made using the name of some particular role, and this role determines the initial access privileges for commands issued in that connection.  

Log in:  
```sql
psql -U username/rolename
//or
SET ROLE role //or user, needs permission to set a role;
```

Current user and session:  
```sql
SELECT SESSION_USER, CURRENT_USER;
```

The REVOKE command revokes previously granted privileges from one or more roles. The key word PUBLIC refers to the implicitly defined group of all roles, Note that any particular role will have the sum of privileges granted directly to it, privileges granted to any role it is presently a member of, and privileges granted to PUBLIC. Thus, for example.  
Similarly, revoking SELECT from a user might not prevent that user from using SELECT if PUBLIC or another membership role still has SELECT rights.  
by default public(which is a role every other role has) access to all databases, so we ant to revoke that:  

```sql
REVOKE ALL PRIVILEGES ON kinds FROM public;// public acts like a user which refrences to all roles and users
//or
REVOKE INSERT/DELETE/ALL/... ON table-name/ DB-name/... FROM PUBLIC//role;
```

We can use GRANT to give permissions to DB objects:The possible privileges are:  

```sql
SELECT
INSERT
UPDATE
DELETE
TRUNCATE
REFERENCES
TRIGGER
CREATE
CONNECT
TEMPORARY
EXECUTE
USAGE
TEMP
ALL
//There is no need to grant privileges to the owner of an object//database object like table, column, view, foreign table, sequence, database,...
```


So we need to revoke the default public access to DBs otherwise all other roles would access all DBs:  
```sql
REVOKE ALL ON DATABASE northwind FROM public;

```

Levels of privileges:  
We can specify whether privileges of a role would be inherited to roles that created with that role using INHERIT or NONINHERIT  

```sql
CREATE ROLE joe LOGIN INHERIT;
CREATE ROLE admin NOINHERIT;
CREATE ROLE wheel NOINHERIT;
GRANT admin TO joe;  
GRANT wheel TO admin; // now joe doesnt have wheel privs

```

## OTHER LEVELS

DB level:  

Permissions:  

- CREATE: new schemas
- CONNECT: connect to a DB/ log in into a
- TEMP: create temporary tables  

Schema level:  

- CREATE: put objs into a schema
- USAGE: look into schema  

by def public has access to create tables in public schema so we might need to revoke it by:

```sql
REVOKE ALL ON SCHEMA public ON public;
```

Table level:  

```txt
SELECT
INSERT  
UPDATE
DELETE
TRUNCATE: removes all data at once
REFERENCES: aloww creatin FKEY const

//
GRANT SELECT ON TABLE t-n TO role-name;  

```

*We can use "ON ALL TABLES IN SCHEMA public ", so we do not need to specify one by one*  

Column level:  

SELECT,INSERT, UPDATE, REFRENCRS(permission to refer a col as FKEY) in specific column  

```
GRANT SELECT (col1,col2,col3) ON t-n TO role-name;
//must not have select on tables as well
```

Row level:  
By def row level securiy is disable so we might need tp turn it on:  

```sql
ALTER TABLE name ENABLE ROW LEVEL SECURITY
```

after this pervious permissions wont work  
Now, row level security needs a new POLICY to a role, more than one policy ~ all added ones or use RESTRECTIVE clause at the end which means ~ AND  

```
CREATE POLICY name ON t-n
[AS RESTRICTIVE]
FOR SELECT//permission TO role
USING (cond)
```

*The messages are not helpful enough sometimes*

We can use these two as well, but I don't dive into them  

```sql
ALTER ROLE  

DROP ROLE  
```