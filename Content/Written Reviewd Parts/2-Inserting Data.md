# ROWS MODIFICATION

## INSERT

We can insert data into a table:  

```sql
INSERT INTO t-n
(col1,col2,...)
VALUES (val1,val2,...),(val1,val2,...)
```

Rows are inserted one at time, auto increment input doesn't need explicit column input, if we don't specify cols clause ((cal1,cal2,...)) inputs go into columns correspondingly.  
We can request DEFAULT value explicitly, or we can store the rows of a query into table:  

```sql
INSERT INTO t-n
(col1,col2,...)
SELECT statement;
```

## MODIFY 

For changing existing rows we use update clause:  

```sql
UPDATE t-n
SET col1=val1 , col=val2 , ...
WHERE cond;
```

Values can be any expression or function call, without condition all rows will be changed  

## DELETE

```sql
DELETE FROM t-n
WHERE cond;
```

No condition means all rows, condition will be finally expressed as 0 or 1(delete), using returning clause we can see what we have deleted, or modified.  

## CREATING NEW TABLE FROM A QUERY

Using this syntax we can store a query result into a new table with rows from an existing table;  

```sql
SELECT cols
INTO new-table
FROM old-table
WHERE cond;
```
*Its like a simple valid query*  

Or alternatively we  can store that data into an existing table using INSERT INTO clause which needs to have same cols and datatypes.  