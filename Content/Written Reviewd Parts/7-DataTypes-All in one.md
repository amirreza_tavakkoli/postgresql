# BASIC DATA TYPES

## TYPES

We have countless number of datatypes in postgres, literally for any need, there's a [datatypes](https://www.postgresql.org/docs/12/datatype.html):  

- Numeric types

- Char 

- Monetary

- Binary

- Date/time

- Boolean

- Enum

- Geometry

- Arrays

- Network/Domain

- Bit string

- JSON

- XML 
- ...

So lets dive into them.  

### NUMERIC TYPES

Numeric types consist of two-, four-, and eight-byte integers, four- and eight-byte floating-point numbers, and selectable-precision decimals.  

Int: smallint(16bit), integer(32bit), and bigint(64bit) store whole numbers, Attempts to store values outside of the allowed range will result in an error.  

Arbitrary: Precision Numbers, Calculations with Arbitrary numeric values yield exact results.  
NUMERIC(precision, scale)/precision: significant number/scale :count of decimal digits in the fractional part.  

without any precision or scale creates a column in which numeric values of any precision and scale can be stored, up to the implementation limit on precision.  

If the scale of a value to be stored is greater than the declared scale of the column, the system will round the value, if the number of digits to the left of the decimal point exceeds the declared precision minus the declared scale, an error is raised.  

*special value NaN, means “not-a-number”. Any operation on NaN yields another NaN. must put quotes around it*  

decimal ~ numeric ~ 16 bit in scale, precision.  

Floating point: The data types real and double precision are inexact, IEEE Standard 754, inexact means ~ stored as approximations.  

Real ~ C's float ~ 6 significant figures and 37 exponent
Double ~ 15 significant figures, 305 exponent.  

Values that are too large or too small for its will cause an error. Rounding might take place if the precision of an input number is too high.  

floating-point types have several special values(use quotes around them):  

Infinity  
-Infinity  
NaN  

*Both of them are driven from float(p) which p is 1 to 53*  

Serial types: smallserial, serial and bigserial are nonnegative numbers best for counting, and a notational convenience for creating unique identifier columns.  

To insert the next value of the sequence into the serial column, specify that the serial column should be assigned its default value. This can be done either by excluding the column from the list of columns in the INSERT statement, or through the use of the DEFAULT key word.  

### MONETARY

64 bits precise number, best for storing money types, works with int and float litralls too, has only 2 fractional digit.  

Input sample ~  '$1,000.00'  

### CHAR

all between ''
character=varying(n), varchar(n)	variable-length with limit  

character(n), char(n)=	fixed-length, blank padded  

text= variable unlimited length  

*character(n) is usually the slowest of the three *  

There are two other fixed-length character types in PostgreSQL, which are internal types, NAME and "char"  

## DATE TIME

*Inputting and outputting date and time is too versatile in nature*

DATE, TIME, TIMESTAMP  

Date and time input is accepted in almost any reasonable format, (you can see accepted samples on documentation) including ISO 8601, SQL-compatible  

Values must be enclosed in single quotes  

TIME, TIMESTAMP : can get a number as precision after second(time(n)), and can have time zones or not  
Date order can be set using "SET DATESTYLE = 'ISO,MDY'" or DMY or ...  
INPUTTING  
TIME:  
04:12:12  
03:12 AM PM  
2:12:12:121  

DATE: 2018-10-12  
TIMESTAMP: 2018-03-21 04:12:31  

Special datetime inputs:
epoch(1970),infinity, - infinity, now, today,tomorrow,yesterday.allbaslls  
you must cast them using ::abstime  

Specifying time zones for time and timestamp:  
Formats:  
full zone name(includes daylight savings) , abbreviation, posix-style+offset  
You can see timezones using:  

```sql
SELECT * FROM pg_timezone_names;
```

Specifying: just add timestamp name or abbrv with data, column must be initilazed with "WITH TIME ZONE" clause  

Current session time zone:  
"SHOW time zone;"  
"SET TIME ZONE 'name'"  

### Interval

Basic: interval  
Advanced: interval[fields][percision] // field are ranges like  YEAR MONTH MINUTE SECOND ...  

The output format of the interval type can be set to one of these styles Sql_standard, postgres, postgres_verbose, or iso, using the command SET intervalstyle. The default is the postgres format.

Format: quantity unit quantity unit ... [dir]  
SQL ~ '4 32:12:12' 4 days 32 hours 12 mins 12 secs  ~ both acceptable  

dir~ ago- or blank+  

'200:122' 200 years 122 months  

DATE ARITHMETIC:  
we can add/sub time, date, timestamps with intervals, output depends on the interval  
we can add/sub two dates, gives back interval  
we can add/sub int(day) to date  

### ARRAYS

PostgreSQL allows columns of a table to be defined as variable-length multidimensional arrays. Arrays of any built-in or user-defined base type.  
Size is ignored in postgres, dimension using [][][], use ARRAY kw, Example in table creation:  

```sql
CREATE TABLE t-n(
col1 int[];
)

```

Inputting values:  

We within curly braces and separate them by commas  

```sql
'{val,val,...}' //text ~ ""
//or 
ARRAY[val1,val2,...] // text in ' '
```

*To set an element of an array constant to NULL, write NULL for the element value*  

accessing:  
using indexes, numbers in [] starting from 0, or range [low:up], we change arrays values using indexes and ranges  

Size:  
array_dims(array-name)  
aaray_lentgh(array-name, dim) // specific dimension  

Searching in arrays:  

```
value = ANY(col)
```