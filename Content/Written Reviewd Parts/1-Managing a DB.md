# DATABASE MANAGEMENT

## CREATING A DATABASE

For creating a new DB from scratch, in which we can issue our queries(anywhere we can run a query in SQL):  

```SQL
CREATE DATABASE db-name;
```

Or, alternatively we can use CLI utility(this command is a wrapper around previous query, used in psql)):  

```SQL
createdb db-name;
```

Finally you can create a new DB using GUI utility(by right clicking on DBs list and click on create)  

----

## DROPPING A DATABASE

Exactly opposite way of above commands:  

```SQL
DROP DATABSE db-name;
```

And:

```SQL
DROPDB db-name
```

Click on any DB on DBs list hit DROP/DELETE  

----

## EXTRA THINGS TO KNOW

- There's a DB called Postgres which is always there, cant be destroyed ,and when we have no other DB we can issue our queries there.  

- Of course we got to have 'create DB' privilege or 'SUPERUSER' role (more on that later in roles)  

- By default we have 2 templates: template0 template1(def), they are raw DBs. and when we create a new DB we actually use them as template for our DB.  

- Template0 is 100% raw(virgin, no added object) ,but template1 has some objects(added previously).  

- We can use other DBs as template, that would be like coping a DB from scratch using this option(no grants, no  DB configurations):  

    ```SQL
    CREATE DATABASE db-name
           [ TEMPLATE [=] template ]
    ```

- You can specify an owner by:  

```sql
CREATE DATABASE name
    [ OWNER [=] user_name ]
```

- This error “could not initialize database directory” is most likely related to insufficient permissions on the data directory.  

- If you want to copy a DB it's better to use 'COPY DATABASE' command.  

- This command can't be in transactional block(must be alone).  

- This command is PostgreSQL extension.  

- It can only be executed by the database owner, it cannot be executed while you or anyone else are connected to that database. (use postgres).  

- Other options:  

```sql
CREATE DATABSE  name

ENCODING [=] encoding //character sets ,encoding name can be found in documentation

IS_TEMPLATE [=] istemplate // can it be used as a template or not

CONNECTION LIMIT [=] connlimit // max number of concurrent connection, -1 def

[ LC_COLLATE [=] lc_collate ]//Collation order- must be match with encoding(utf8 is matched with any other one)

           [ TABLESPACE [=] tablespace_name ] // DB tablespace
```

## ALTER DATABASE

ALTER DATABASE changes the attributes of a database (you must have the given power to that(role)):  

```sql

ALTER DATABASE name RENAME TO new_name
// rename
ALTER DATABASE name OWNER TO new_owner-username
// new owner, must be a member of that role or group
ALTER DATABASE name SET TABLESPACE new_tablespace
// new tablespace
```