# SEQUENCES

## BASICS

Generates unique identifier for id fields:  

```sql
CREATE SEQUENCE s-name;
```

By def starts with 0, using some sequence functions we can see current, next value or we can set some value into that:  

```sql
SELECT nextval('name')// others: curval,lastval,setval('name',start,option) // option ~ false or true
```

*cant be brought back*  

We can specify values of a seq using this option while creating:  

- INCREMENT  

- MINVALUE  

- MAXVALUE  

- STARTS WITH  

*We can see a list of seqs in GUI drop menu*  

Now we need to attach it to a field:  

```
ALTER TABLE t-n
ALTER col-name SET DEFAULT nextval('seq-name');
```

We can specify OWNED BY clause too, we can create a temporary seq, we can use other datatypes for seq and we can make it a circular seq and...  
Now every row we want to insert, will be auto increment (better to have pkey cons on that field)  

We can modify our seq using ALTER SEQUENCE, we can rename it, change values, restart it,...  

deletion using DROP SEQUENCE s-name;  

## SERIAL

Using serial datatype as datatype creates an auto-increment field id which is really efficient, and adds not null constraint.  

