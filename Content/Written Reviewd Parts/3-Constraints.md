# CONSTRAINTS

## DEF

Constraints control what kind of data can go into a specific field, Datatypes are checked first before any other constraints, if constraints get violated an error will raise.  
We have 2 types of constraints (cons~constraints):

- Column cons
- table cons

And every cons has a name, you can specify that directly or let DB name that. a full list of DBs of a table can be found in GUI>>CONSTRAINTS  
You can specify them while you are creating a table or you can add them afterwards(using ALTER TABLE)
Basic Form:

```sql
CREATE TABLE t-n(
col1 datatype cons1 cons2, // we can have multiple const at once
col2 datatype cons
...
)
```

## TYPES

- NOT NULL: by def values  are NULL or driven by default cons, so if you want them not to be null when they're getting inserted into table ,use it(only column cons available).

```sql
CREATE TABLE t-n(
col1 datatype NOT NULL);
```

Drop or add:

```sql
    ALTER TABLE t-n
    ALTER COLUMN  column_name { SET | DROP } NOT NULL
```

*The NOT NULL constraint has an inverse: the NULL constraint. This does not mean that the column must be null*  

- UNIQUE: the table cant have duplicated values, every row has unique value or null, great for id fields.

On creation:

```sql
CREATE TABLE t-n(
col1 datatype UNIQUE);

```
***We can give cons name using "CONSTRAINT Cc-Name" after datatype***

After Creation (using table Cons):

```sql
ALTER TABLE t-n
ADD CONSTRAINT cons-n UNIQUE(col1,col2,...)
```

*By def adds a B-TREE index, you can see all cons in GUI*

PKEY : combination of unique and not null, perfect for id fields, by def adds an index
In Creation: exactly like unique  
After creation:
```
ALTER TABLE t-n
ALTER COLUMN  ADD CONSTRAINT c-n PRIMARY KEY(COLS) //its a table con
```

Dropping : 

```sql
ALTER TABLE t-n
DROP CONSTARINTS c-n
```

FKEY: for creating a relation between two table, table cons mode is better with FKEY
Creating and dropping: Exactly like pkey:
```
ALTER TABLE
ADD CONSTRAINTN con-name FOREIGN KEY(col)//refrences(A) REFRENCES table-name(col) //refrenced(b)

```
datatype and name must match between these two  
options for drooping a reference column:  
CASCADE(delete in A), RESTRICT(prevent deletion), NO ACTION(do nothing)

Default:
Gives a def value to new rows,by def if not expressed ~ null
after declaring datatype use DEFAULT val/exp;
Change or drop:
```
ALTER TABLE t-n
ALTER c-n
DROP DEAFULT
//or
SET DEFAULT val;
```

Check:
the most generic constraint type, Checks a condition to be met, cond must pass true, false means error, we can use other columns in condition:

```sql
CREATE TABLE t-n(
col datatype check(cond)
)
```

we can specify a name.  
or

```sql
ALTER TABLE t-n
ADD CONSTRAINT con-n CHECK (cond);
//
DROP con-name
```


