# TRANSACTIONS

## BASICS

Every command we query, is in a transaction block, meaning that each individual statement has an implicit BEGIN and (if successful) COMMIT wrapped around it, but we might not need to write it explicitly.  
A group of statements surrounded by BEGIN and COMMIT is sometimes called a transaction block.  
Until now we only had one-statement commands, meaning only one query is being queried at a certain moment, but,if you want to have multiple statements being queried at once, you need to bundle them together using transactions.  
We normally use BEGIN and COMMIT around a transaction(block) for specifying the start and the end of a transaction(multiple statements).  
Every transaction needs to have some property which we use ACID as an acronym for those.  

## TRANSACTION'S PROPERTIES

A:ATOMICITY, meaning whether all the statements and operations work or none of them works, everything is like a single unit.  
As an example:money transaction in a bank:  

```sql
UPDATE accounts SET balance = balance - 100.00
    WHERE name = 'Alice';
UPDATE branches SET balance = balance - 100.00
    WHERE name = (SELECT branch_name FROM accounts WHERE name = 'Alice');
UPDATE accounts SET balance = balance + 100.00
    WHERE name = 'Bob';
```

So whether both transactions are completed or none of them,otherwise we would encounter a problem.  

C:Consistency, meaning DB state and rows are changed after a successful transaction, guaranteed that you did that works right.  

I:Isolation, TRs wont interface each other, nothing happened in the middle of a TR.  
and TRs don't know about each other's content.  

D:Durability, the changes after COMMITs will be stated properly, system failure or crashes can't lead to lost data.  

So successful TR ~ permeant save(change)  

*It's a DB concept not SQL language.*  

## SIMPLE TR

```sql
//postgres standard
BEGIN;
-s1
-s2
-s3
END; 

//or SQL standard

START;
-s1
-s2
-s3
COMMIT;
```

*if an error occurs in second part, it wont run first part*  


**Rollback**:nothing will be done if we specify ROLLBACK; in TR block:  

```sql
START;
-s1
-s2
ROLLBACK;//s1 and s2 will not be done
```

Savepoint: partial save, for partial rollback, so you ROLLBACK TO savepoint-name:  

```sql
START;
-s1
SAVEPOINT s-name;
-s2
ROLLBACK TO s-name;//s1 and s2 will not be done
```

### ISOLATION

Can others see partial result before TR finishes?Isolation defines how/when changes made by one partial operation(statement) become visible to other TRs or sessions.  
We have 3 phenomenon happening during a TR:  

- Dirty read: one TR reads unfinished TR results(data may be rolled back)  

-Nonrepeatable read: during a TR data in a row retrieved twice, and differ with each  

-Phantom read: rows are added or deleted in course of a TR  

We have 4 levels of isolation in SQL standards:  

- Read uncommitted: ALLOWS ALL OF THE 3 phenomenon  
*FASTEST LEAST SAFE, DOESNT LOCK UP DB*  

- Read committed: no dirty reads, guarantees commitment of data,,(rows data can be changed)  
 
- Repeatable reads: only phantom reads, only rows can be added or deleted, rows are fixed  

- Serialized: safest, any concurrent execution of a set of Serializable transactions is guaranteed to produce the same effect as running them one at a time in some order.  

------

#### **ISOLATION IN POSTGRESQL**

PGSQL uses MVCC (to provide concurrent access to the database), meaning it wont lock the DB(which might cause problems), changes are timestamped, any statements sees a snapshot of data at given timestamp.  

**Requesting an isolation level:**
PGSQL doesn't allow dirty reads, and first two and last two levels ate the same because of MVCC architecture.  
Serializable~Repeatable read(nothing allowed) , Read committed~Read uncommitted (only dirty reads are not allowed)  
def: read committed  

Transaction access mode: determines whether the transaction is read/write or read-only. Read/write is the default. When a transaction is read-only, the following SQL commands are disallowed: INSERT, UPDATE, DELETE, and COPY FROM  

```sql

// begginiing
START TRANSACTION [ transaction_mode]

where transaction_mode is one of:

    ISOLATION LEVEL { SERIALIZABLE | REPEATABLE READ | READ COMMITTED | READ UNCOMMITTED }

    // or after start of a TR

    SET TRANSACTION level; 
```

The SET TRANSACTION "SNAPSHOT  snapshot identifier" command allows a new transaction to run with the same snapshot as an existing transaction.  